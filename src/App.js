import React, { Component } from 'react';
import './App.css';
import Header from "./Components/Header";
import About from "./Components/About";
import Resume from "./Components/Resume";
import Porfolio from "./Components/Porfolio";
import Testimonials from "./Components/Testimonials";
import Contact from "./Components/Contact";
import Footer from "./Components/Footer";

class App extends Component {
  render() {
    return (
        <div className="App">
            <Header />
            <About />
            <Resume />
            <Porfolio />
            <Testimonials />
            <Contact />
            <Footer />
        </div>
    );
  }
}

export default App;
